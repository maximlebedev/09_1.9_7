package controller;

import dao.ServiceDao;
import entity.Service;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Pattern;

public class MainMenuViewController {

    @FXML
    private Button buttonRecord;
    @FXML
    private Button buttonViewRecord;
    @FXML
    private Button buttonAddService;
    @FXML
    private Text infoField;
    @FXML
    private VBox vBox;
    @FXML
    private Button buttonAdministrator;
    @FXML
    private TextField searchField;

    private boolean adminMode = false;
    ServiceDao serviceDao = new ServiceDao();
    ArrayList<Service> services = new ArrayList<>();

    public void initialize() throws IOException, SQLException {
        vBox.getChildren().clear();
        fillServices();
        fillScrollPane();
    }

    private void setInfoField(int currentServices) throws SQLException {
        infoField.setText("Показано " + currentServices + " из " + serviceDao.getTheNumberOfService() + " сервисов");
    }

    private void fillServices() throws SQLException {
        services.clear();
        for (int id = 1; id <= serviceDao.getLastIdOfService(); id++) {
            try {
                Service service = new Service();
                service.setId(id);
                services.add(service);
            } catch (SQLException ignored) {
            }
        }
    }

    private void fillScrollPane() throws IOException, SQLException {
        int serviceCount = 0;
        for (Service service : services) {
            serviceCount++;
            addPaneToScrollPane(service);
        }
        setInfoField(serviceCount);
    }

    private void addPaneToScrollPane(Service service) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/serviceView.fxml"));
        Pane pane = loader.load();
        ServiceViewController serviceViewController = loader.getController();
        serviceViewController.setService(service);
        if (adminMode) {
            serviceViewController.enterIntoAdministratorMode();
        }
        vBox.getChildren().addAll(pane);
    }

    void changeMode() throws IOException, SQLException {
        adminMode = true;
        buttonRecord.setVisible(true);
        buttonViewRecord.setVisible(true);
        buttonAddService.setVisible(true);
        Stage stage = (Stage) buttonAdministrator.getScene().getWindow();
        stage.setTitle("Ремонтный сервис (режим администратора)");
        initialize();
    }

    private void sortByDiscount(int min, int max) throws IOException, SQLException {
        vBox.getChildren().clear();
        int serviceCount = 0;
        for (Service service : services) {
            if (min <= service.getDiscount() && service.getDiscount() < max) {
                addPaneToScrollPane(service);
                serviceCount++;
            }
        }
        setInfoField(serviceCount);
    }

    @FXML
    private void search() throws IOException, SQLException {
        vBox.getChildren().clear();
        int serviceCount = 0;
        if (searchField.getText().equals("")) {
            initialize();
        } else {
            for (Service service : services) {
                if (Pattern.matches(".*" + searchField.getText().toLowerCase() + ".*", service.getNameService().toLowerCase())) {
                    serviceCount++;
                    addPaneToScrollPane(service);
                }
            }
        }
        setInfoField(serviceCount);
    }

    @FXML
    private void enterInAdministratorMode() throws IOException {
        if (adminMode) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Вы уже в режиме разработчика");
            alert.setHeaderText(null);
            alert.setContentText("Вы уже успешно перешли в режим администратора");
            alert.showAndWait();
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/changeModeView.fxml"));
            Parent root = loader.load();
            ChangeModeViewController changeModeViewController = loader.getController();
            changeModeViewController.getMainController(this);
            Stage stage = new Stage();
            stage.setTitle("Переход в режим администратора");
            stage.setScene(new Scene(root, 300, 50));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        }
    }

    @FXML
    private void sortByPriceIncrease() throws IOException {
        vBox.getChildren().clear();
        ArrayList<Service> serviceArrayList = new ArrayList<>(services);
        Comparator<Service> comparator = Comparator.comparingDouble(Service::getCost);
        serviceArrayList.sort(comparator);
        for (Service services : serviceArrayList) {
            addPaneToScrollPane(services);
        }
    }

    @FXML
    private void sortByPriceReduction() throws IOException {
        vBox.getChildren().clear();
        ArrayList<Service> serviceArrayList = new ArrayList<>(services);
        Comparator<Service> comparator = Comparator.comparingDouble(Service::getCost).reversed();
        serviceArrayList.sort(comparator);
        for (Service services : serviceArrayList) {
            addPaneToScrollPane(services);
        }
    }

    @FXML
    private void sortByDiscountFirst() throws IOException, SQLException {
        sortByDiscount(0, 5);
    }

    @FXML
    private void sortByDiscountSecond() throws IOException, SQLException {
        sortByDiscount(5, 15);
    }

    @FXML
    private void sortByDiscountThird() throws IOException, SQLException {
        sortByDiscount(15, 30);
    }

    @FXML
    private void sortByDiscountFourth() throws IOException, SQLException {
        sortByDiscount(30, 70);
    }

    @FXML
    private void sortByDiscountFifth() throws IOException, SQLException {
        sortByDiscount(70, 100);
    }

    @FXML
    private void sortByDiscountReset() throws IOException, SQLException {
        initialize();
    }

    @FXML
    private void record() {
    }

    @FXML
    private void viewRecord() {
    }

    @FXML
    private void addService() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/addServiceView.fxml"));
        Parent root = loader.load();
        AddServiceViewController addServiceViewController = loader.getController();
        addServiceViewController.setController(this);
        Stage stage = new Stage();
        stage.setTitle("Добавление новой услуги");
        stage.setScene(new Scene(root, 600, 400));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }
}
