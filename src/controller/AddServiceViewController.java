package controller;

import dao.ServiceDao;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class AddServiceViewController {

    @FXML
    private TextField nameService;
    @FXML
    private TextField cost;
    @FXML
    private TextField durationInMinute;
    @FXML
    private TextField description;
    @FXML
    private TextField discount;
    @FXML
    private TextField pathToImage;
    private MainMenuViewController controller;

    @FXML
    private void addService() throws SQLException, IOException {
        if (checkingForEmptiness()) {
            ServiceDao serviceDao = new ServiceDao(nameService.getText(), Integer.parseInt(durationInMinute.getText()) * 60,
                    Integer.parseInt(cost.getText()), Integer.parseInt(discount.getText()), description.getText(), pathToImage.getText());

            serviceDao.insertServiceIntoTable();
            Stage stage = (Stage) nameService.getScene().getWindow();
            stage.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Сервис добавлен");
            alert.setHeaderText(null);
            alert.setContentText("Сервис был успешно добавлен");
            alert.showAndWait();
            controller.initialize();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Сервис не добавлен");
            alert.setHeaderText(null);
            alert.setContentText("Сервис не был добавлен, пустые значения");
            alert.showAndWait();
        }
        controller.initialize();
    }

    private boolean checkingForEmptiness() {
        return !nameService.getText().equals("") && !cost.getText().equals("") && !durationInMinute.getText().equals("")
                && !discount.getText().equals("");
    }

    public void setController(MainMenuViewController controller) {
        this.controller = controller;
    }
}
