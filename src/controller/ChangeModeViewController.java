package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class ChangeModeViewController {

    @FXML
    private TextField codeField;
    private MainMenuViewController controller;

    @FXML
    private void checkCode(ActionEvent actionEvent) throws IOException, SQLException {
        if (codeField.getText().equals("0000")) {
            controller.changeMode();
            Stage stage = (Stage) codeField.getScene().getWindow();
            stage.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Код верный");
            alert.setHeaderText(null);
            alert.setContentText("Вы успешно перешли в режим администратора");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Код неверный");
            alert.setHeaderText(null);
            alert.setContentText("Пароль не верный, не удалось перейти в режим администратора");
            alert.showAndWait();
        }
    }

    void getMainController(MainMenuViewController controller) {
        this.controller = controller;
    }
}
