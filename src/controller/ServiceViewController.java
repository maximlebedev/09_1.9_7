package controller;

import entity.Service;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ServiceViewController {

    @FXML
    private HBox hBox;
    @FXML
    private Text discountField;
    @FXML
    private Text informationAboutTheServiceField;
    @FXML
    private Text serviceNameField;
    @FXML
    private ImageView imageField;
    @FXML
    private Button editButton;
    @FXML
    private Button deleteButton;

    private Service service;

    public void setService(Service service) throws FileNotFoundException {
        this.service = service;
        setImageField();
        setServiceNameField();
        setInformationAboutTheServiceField();
        setDiscountField();
    }

    @FXML
    private void edit() {
    }

    @FXML
    private void delete() {
    }

    private void setImageField() throws FileNotFoundException {
        imageField.setImage(new Image(new FileInputStream(service.getPathToImage())));
    }

    private void setServiceNameField() {
        serviceNameField.setText(service.getNameService());
    }

    private void setInformationAboutTheServiceField() {
        informationAboutTheServiceField.setText(service.getInformationAboutTheService());
    }

    private void setDiscountField() {
        if (!(service.getDiscount() == 0)) {
            discountField.setText("* Скидка " + service.getDiscount() + "%");
            hBox.setStyle("-fx-background-color: #90ee90");
        } else {
            discountField.setText("");
        }
    }

    protected void enterIntoAdministratorMode() {
        editButton.setVisible(true);
        deleteButton.setVisible(true);
    }
}
