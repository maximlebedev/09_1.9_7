package database;

import java.sql.*;

public class DataBase implements DataBaseConfig {

    private static Connection connection;
    public static Statement statement;

    static {
        try {
            connection = setConnection();
            statement = getStatement();
        } catch (Exception ignored) {
        }
    }

    private static Connection setConnection() throws Exception {
        Connection connection;
        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        return connection;
    }

    private static Statement getStatement() throws SQLException {
        statement = connection.createStatement();
        return statement;
    }

    public static Connection getConnection() {
        return connection;
    }
}
