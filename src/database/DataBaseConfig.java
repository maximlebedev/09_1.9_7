package database;

public interface DataBaseConfig {

    String URL = "jdbc:mysql://localhost:3306/car_service?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
