package parse;

import dao.ClientDao;
import java.io.*;

public class ParseClient {

    public void parseClientFile() {
        String fileName = "client_a_import.txt";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String data = bufferedReader.readLine();
            while (data != null) {
                String[] parseData = data.split(", ");
                ClientDao clientDao = new ClientDao(parseData[0], parseData[1], parseData[2],
                        parseData[3], parseData[5], parseData[7], parseData[6], parseData[4]);
                clientDao.insertClientIntoTable();
                data = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
