package parse;

import dao.ServiceDao;
import dao.ServicePhotoDao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseService {

    public void parseServiceFile() {
        String fileName = "service_a_import.csv";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            bufferedReader.readLine();
            String data = bufferedReader.readLine();
            String regEx = "[0-9]+";
            int id = 1;
            while (data != null) {
                String[] parseData = data.split("; ");
                Pattern pattern = Pattern.compile(regEx);
                Matcher matcherDuration = pattern.matcher(parseData[2]);
                Matcher matcherCost = pattern.matcher(parseData[3]);
                Matcher matcherDiscount = pattern.matcher(parseData[4]);

                int duration = 0;
                int cost = 0;
                int discount = 0;


                while (matcherDuration.find()) {
                    duration = Integer.parseInt(parseData[2].substring(matcherDuration.start(), matcherDuration.end()));
                }
                while (matcherCost.find()) {
                    cost = Integer.parseInt(parseData[3].substring(matcherCost.start(), matcherCost.end()));
                }
                while (matcherDiscount.find()) {
                    discount = Integer.parseInt(parseData[4].substring(matcherDiscount.start(), matcherDiscount.end()));
                }

                if (duration < 24) {
                    duration *= 3600;
                } else {
                    duration *= 60;
                }

                ServiceDao serviceDao = new ServiceDao(parseData[0], duration,
                        cost, discount, null, parseData[1].replace("\\", "/"));
                ServicePhotoDao servicePhotoDao = new ServicePhotoDao();
                serviceDao.insertServiceIntoTable();
                servicePhotoDao.insertPhotoPathIntoTable(id, parseData[1].replace("\\", "/"));
                id++;
                data = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
