package parse;

import dao.ServiceClientDao;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sun.util.calendar.BaseCalendar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

public class ParseServiceClient {

    public void parseServiceClientFile() throws FileNotFoundException {
        String fileName = "serviceclient_a_import.xlsx";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(file))) {
            XSSFSheet sheet = workbook.getSheet("Sheet1");

            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                XSSFRow row = sheet.getRow(i);
                String service = row.getCell(0).getStringCellValue();
                Date date = row.getCell(1).getDateCellValue();
                String client = row.getCell(2).getStringCellValue();

                String year = String.valueOf(date.getYear() + 1900);
                String day = String.valueOf(date.getDay() + 14);
                String month = String.valueOf(date.getMonth() + 1);
                String hour = String.valueOf(date.getHours());
                String minute = String.valueOf(date.getMinutes());

                String sqlDate = year + "-" + normalize(month) + "-" + normalize(day) + " " + normalize(hour)
                        + ":" + normalize(minute) + ":00.000000";
                ServiceClientDao serviceClientDao = new ServiceClientDao(client, service, sqlDate);
                serviceClientDao.insertClientServiceIntoTable();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String normalize(String number) {
        if (Integer.parseInt(number) < 10) {
            return "0" + number;
        }

        return number;
    }
}
