package dao;

import database.DataBase;

import java.sql.SQLException;
import java.sql.Statement;

public class ServiceClientDao {


    private final String lastName;
    private final String serviceTitle;
    private final String startTime;

    public ServiceClientDao(String lastName, String serviceTitle, String startTime) {
        this.lastName = lastName;
        this.serviceTitle = serviceTitle;
        this.startTime = startTime;
    }

    public void insertClientServiceIntoTable() {

        try {
            String query = "INSERT INTO `clientservice` (`ID`, `ClientLastname`, `ServiceTitle`, `StartTime`, `Comment`)" +
                    " VALUES (NULL, '" + lastName + "', '" + serviceTitle + "', '" + startTime + "', NULL) ";
            Statement statement = DataBase.getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void getAllData() {
        System.out.println("Фамилия: " + lastName);
        System.out.println("Сервис: " + serviceTitle);
        System.out.println("Время: " + startTime);
    }
}
