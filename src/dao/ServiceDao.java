package dao;

import database.DataBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ServiceDao {

    private String title;
    private int duration;
    private int cost;
    private int discount;
    private String path;
    private String description;

    public ServiceDao() {
    }

    public ServiceDao(String title, int duration, int cost, int discount, String description, String path) {
        this.title = title;
        this.duration = duration;
        this.cost = cost;
        this.discount = discount;
        this.description = description;
        this.path = path;
    }

    public void insertServiceIntoTable() {
        try {
            if (description.equals("")) {
                description = null;
            }
            if (path.equals("")) path = "Услуги автосервиса\\\\service_logo.png";
            String query = "INSERT INTO `service` (`ID`, `Title`, `Cost`, `DurationInSeconds`, `Description`, `Discount`, `MainImagePath`)" +
                    " VALUES (NULL, '" + title + "', '" + cost + "', '" + duration + "', '" + description + "' , '" + discount + "', '" + path + "')";
            Statement statement = DataBase.getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void insertPathIntoTable(int id) {
        try {
            String query = "INSERT INTO `servicephoto` (`ID`, `ServiceID`, `PhotoPath`) VALUES (NULL, '" + id + "', '" + path + "')";
            Statement statement = DataBase.getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public String getPathToImage(int id) throws SQLException {
        String query = "SELECT `MainImagePath` FROM `service` WHERE id = " + id;
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return "C:\\Users\\404\\Documents\\Java\\09_1.9_7\\resources\\"
                + resultSet.getString("MainImagePath");
    }

    public int getTheNumberOfService() throws SQLException {
        String query = "SELECT COUNT(1) FROM service";
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getInt("COUNT(1)");
    }

    public int getLastIdOfService() throws SQLException {
        String query = " SELECT MAX(`id`) FROM `service`";
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getInt("MAX(`id`)");
    }

    public String getServiceName(int id) throws SQLException {
        String query = "SELECT `Title` FROM `service` WHERE id = " + id;
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getString("Title");
    }

    public int getCost(int id) throws SQLException {
        String query = "SELECT `Cost` FROM `service` WHERE id = " + id;
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getInt("Cost");
    }

    public int getDuration(int id) throws SQLException {
        String query = "SELECT `DurationInSeconds` FROM `service` WHERE id = " + id;
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getInt("DurationInSeconds") / 60;
    }

    public int getDiscount(int id) throws SQLException {
        String query = "SELECT `Discount` FROM `service` WHERE id = " + id;
        ResultSet resultSet = DataBase.getConnection().createStatement().executeQuery(query);
        resultSet.next();

        return resultSet.getInt("Discount");
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
