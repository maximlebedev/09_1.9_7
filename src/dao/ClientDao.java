package dao;

import database.DataBase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClientDao {

    private final String firstName;
    private final String lastName;
    private final String patronymic;
    private final String gender;
    private final String birthDay;
    private final String registrationDate;
    private final String email;
    private final String telephone;

    public ClientDao(String firstName, String lastName, String patronymic, String gender,
                     String birthDay, String registrationDate, String email, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.gender = gender;
        this.birthDay = birthDay;
        this.registrationDate = registrationDate;
        this.email = email;
        this.telephone = telephone;
    }

    public void getAllData() {
        System.out.println("Имя: " + firstName);
        System.out.println("Фамилия: " + lastName);
        System.out.println("Отчество: " + patronymic);
        System.out.println("Пол: " + gender);
        System.out.println("Дата рождения: " + birthDay);
        System.out.println("Дата регистрации: " + registrationDate);
        System.out.println("Почта: " + email);
        System.out.println("Телефон: " + telephone);
    }

    public void insertClientIntoTable() {
        try {
            int codeGender = 0;
            if (gender.equals("ж")) {
                codeGender = 1;
            }
            String query = "INSERT INTO `client` (`ID`, `FirstName`, `LastName`, `Patronymic`, `Birthday`, " +
                    "`RegistrationDate`, `Email`, `Phone`, `GenderCode`, `PhotoPath`) " +
                    "VALUES (NULL, '" + firstName + "', '" + lastName + "', '" + patronymic +"', '" + birthDay +"', " +
                    "'" + registrationDate +"', '" + email + "', '" + telephone + "', " + codeGender + ", NULL)";
            Statement statement = DataBase.getConnection().createStatement();
            statement.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
