package entity;

import dao.ServiceDao;

import java.sql.SQLException;

public class Service {

    private final ServiceDao serviceDao = new ServiceDao();
    private int id;
    private String nameService;
    private String pathToImage;
    private String informationAboutTheService;
    private double cost;
    private int duration;
    private double discount;

    public void setId(int id) throws SQLException {
        this.id = id;
        setPathToImage();
        setNameService();
        setCost();
        setDuration();
        setDiscount();
        setInformationAboutTheService();
    }

    public void showAllInfo() {
        System.out.println(id);
        System.out.println(pathToImage);
        System.out.println(informationAboutTheService);
        System.out.println(cost);
        System.out.println(discount);
    }

    private void setPathToImage() throws SQLException {
        pathToImage = serviceDao.getPathToImage(id);
    }

    private void setNameService() throws SQLException {
        nameService = serviceDao.getServiceName(id);
    }

    private void setCost() throws SQLException {
        cost = serviceDao.getCost(id);
    }

    private void setDuration() throws SQLException {
        duration = serviceDao.getDuration(id);
    }

    private void setDiscount() throws SQLException {
        discount = serviceDao.getDiscount(id);
    }

    private void setInformationAboutTheService() {
        if (discount == 0) {
            informationAboutTheService = cost + " рублей за " + duration + " минут";
        } else {
            double newCost = cost * (1 - (discount / 100));
            informationAboutTheService = "Старая цена: " + cost + "\nНовая цена: " + newCost + " рублей за " + duration + " минут";
        }
    }

    public String getNameService() {
        return nameService;
    }

    public String getPathToImage() {
        return pathToImage;
    }

    public String getInformationAboutTheService() {
        return informationAboutTheService;
    }

    public int getDiscount() {
        return (int) discount;
    }

    public double getCost() {
        if (!(discount == 0)) {
            return cost * (1 - (discount / 100));
        } else {
            return cost;
        }
    }
}
